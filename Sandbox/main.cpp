#include <algorithm>
#include <SimpleNeuralNetwork/Perceptron.hpp>
#include <iostream>
#include <chrono>

#include "SimpleNeuralNetwork/Activation.hpp"
#include "SimpleNeuralNetwork/MNIST.hpp"
#include "SimpleNeuralNetwork/BreastCancerDataset.hpp"

int main()
{
	std::vector<unsigned> topology = { 28 * 28, 230, 10 };
	snn::Perceptron perceptron(topology, snn::Activation::sigmoid);
	MNIST trainDataSet{ "../SimpleNeuralNetwork/datasets/mnist/train/train-labels-idx1-ubyte", "../SimpleNeuralNetwork/datasets/mnist/train/train-images-idx3-ubyte" };
	std::cout << "Jeu de donnees charge" << std::endl;

	perceptron.train(trainDataSet, 2);
	std::cout << "Fin de l'entrainement" << std::endl;

	const MNIST testDataSet{ "../SimpleNeuralNetwork/datasets/mnist/test/t10k-labels-idx1-ubyte", "../SimpleNeuralNetwork/datasets/mnist/test/t10k-images-idx3-ubyte" };

	std::cout << "Debut des tests" << std::endl;
	const auto accuracy = perceptron.test(testDataSet, snn::Problem::Classification);
	std::cout << "taux de reussite: " << accuracy*100 << "%" << std::endl;


	//perceptron.write("network.net");

	std::cout << "/-----------------------------------------------/" << std::endl;
	std::cout << "		Reconnaissance de chiffre" << std::endl;
	std::cout << "/-----------------------------------------------/\n" << std::endl;

	bool c = true;
	
	while (c) {
		std::cout << "Entrez le lien vers le fichier (q si vous voulez quitter) : ";
		std::string filename;
		std::cin >> filename;
		if (filename == "q") break;

		std::ifstream file{ filename };
		// On supprime les informations inutiles (taille de l'image, tag de gimp, etc.)
		std::string useless;
		file >> useless;
		file >> useless;
		file >> useless;
		file >> useless;
		std::vector<float> input(28 * 28);
		for (auto i = 0; i < 28 * 28; i++)
		{
			float temp;
			file >> temp;
			input[i] = temp / 255.f;
		}

		auto output = perceptron.evaluate(input);
		std::cout << "Chiffre reconnu : " << static_cast<char>('0' + std::max_element(output.begin(), output.end()) - output.begin()) << "\n" << std::endl;
	}
	return 0;
}
