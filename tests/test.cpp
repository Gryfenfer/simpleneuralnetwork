#include <chrono>

#include "pch.h"
#include <SimpleNeuralNetwork/Perceptron.hpp>
#include <SimpleNeuralNetwork/MNIST.hpp>
#include <SimpleNeuralNetwork/xor.hpp>
#include "SimpleNeuralNetwork/BreastCancerDataset.hpp"
#include <SimpleNeuralNetwork/Activation.hpp>

#include "SimpleNeuralNetwork/EMNIST.hpp"


TEST(TestCaseName, TestName) {
  EXPECT_EQ(1, 1);
  EXPECT_TRUE(true);
}

TEST(DatasetTests, MNIST)
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	std::vector<unsigned> topology = { 28*28, 230, 10 };
	snn::Perceptron perceptron(topology, snn::Activation::sigmoid);
	MNIST trainDataSet{ "../SimpleNeuralNetwork/datasets/mnist/train/train-labels-idx1-ubyte", "../SimpleNeuralNetwork/datasets/mnist/train/train-images-idx3-ubyte" };
	std::cout << "Loaded training data set" << std::endl;

	perceptron.train(trainDataSet, 2);
	std::cout << "End training" << std::endl;

	const MNIST testDataSet{ "../SimpleNeuralNetwork/datasets/mnist/test/t10k-labels-idx1-ubyte", "../SimpleNeuralNetwork/datasets/mnist/test/t10k-images-idx3-ubyte" };

	std::cout << "start testing" << std::endl;
	const auto accuracy = perceptron.test(testDataSet, snn::Problem::Classification);
	std::cout << "accurracy: " << accuracy << std::endl;

	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	std::cout << "finished computation at " << std::ctime(&end_time)
		<< "elapsed time: " << elapsed_seconds.count() << "s\n";
	
	EXPECT_GT(accuracy, 0.85f);
}


TEST(DatasetTests, EMNIST)
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	std::vector<unsigned> topology = { 28 * 28, 230, 26 };
	snn::Perceptron perceptron(topology, snn::Activation::sigmoid);
	EMNIST trainDataSet{ "../SimpleNeuralNetwork/datasets/EMNIST/train/emnist-letters-train-labels-idx1-ubyte", "../SimpleNeuralNetwork/datasets/EMNIST/train/emnist-letters-train-images-idx3-ubyte", 26 };
	std::cout << "Loaded training data set" << std::endl;

	perceptron.train(trainDataSet, 2);
	std::cout << "End training" << std::endl;

	EMNIST testDataSet{ "../SimpleNeuralNetwork/datasets/EMNIST/test/emnist-letters-test-labels-idx1-ubyte", "../SimpleNeuralNetwork/datasets/EMNIST/test/emnist-letters-test-images-idx3-ubyte", 26 };

	std::cout << "start testing" << std::endl;
	const auto accuracy = perceptron.test(testDataSet, snn::Problem::Classification);
	std::cout << "accurracy: " << accuracy << std::endl;

	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	std::cout << "finished computation at " << std::ctime(&end_time)
		<< "elapsed time: " << elapsed_seconds.count() << "s\n";

	EXPECT_GT(accuracy, 0.60f);
}

TEST(DatasetTests, BreastCancerDataset)
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	std::vector<unsigned> topology = { 9, 50, 2 };
	snn::Perceptron perceptron(topology, snn::Activation::sigmoid);
	BreastCancerDataset data{ "../SimpleNeuralNetwork/datasets/breast cancer dataset/data.data" };
	auto [training, testing] = data.split(0.7f); // 70%
	perceptron.train(training, 50000);
	const auto accuracy = perceptron.test(testing, snn::Problem::Classification);
	std::cout << accuracy << std::endl;

	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	std::cout << "finished computation at " << std::ctime(&end_time)
		<< "elapsed time: " << elapsed_seconds.count() << "s\n";
	EXPECT_GT(accuracy, 0.60f);
}

TEST(DatasetTests, XOR)
{
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	std::vector<unsigned> topology = { 2, 10, 10, 1 };
	snn::Perceptron perceptron(topology, snn::Activation::sigmoid);
	Xor dataset;
	perceptron.train(dataset, 60000);
	const auto accuracy = perceptron.test(dataset, snn::Problem::Evaluation);
	std::cout << accuracy << std::endl;

	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	std::cout << "finished computation at " << std::ctime(&end_time)
		<< "elapsed time: " << elapsed_seconds.count() << "s\n";
	EXPECT_GT(accuracy, 0.60f);
}