#pragma once
#include <SimpleNeuralNetwork/Dataset.hpp>
#include <string>

class BreastCancerDataset : public snn::Dataset {

public:
	BreastCancerDataset() = default;
	explicit BreastCancerDataset(const std::string& filename);
	std::pair<BreastCancerDataset, BreastCancerDataset> split(float ratio);
};