#pragma once
#include <SimpleNeuralNetwork/Layer.hpp>
#include <SimpleNeuralNetwork/Dataset.hpp>

namespace snn
{
	enum class Problem
	{
		Classification,
		Evaluation
	};
	
	class Perceptron
	{
	public:
		Perceptron() = delete;
		explicit Perceptron(std::vector<unsigned>& topology, const ActivationFunctions& activationFunctions);
		explicit Perceptron(const std::string& filename, const ActivationFunctions& activationFunctions);

		[[nodiscard]] std::vector<float> evaluate(const std::vector<float>& input);
		void train(Dataset& dataset, const unsigned nbEpoch);
		[[nodiscard]] float test(const Dataset& dataset, const Problem problem);

		void write(const std::string& filename);

	private:
		std::vector<Layer> layers;

		static std::vector<float> computeCost(const std::vector<float>& result, const std::vector<float>& targets);
		void backProp(const std::vector<float>& result, const std::vector<float>& targets);
		void read(const std::string& filename);
	};
}