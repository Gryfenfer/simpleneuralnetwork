#pragma once
#include <functional>

namespace snn
{
	struct ActivationFunctions
	{
		std::function<float(float)> activationFunction;
		std::function<float(float)> activationFunctionDerivative;
	};
}
