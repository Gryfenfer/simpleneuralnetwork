#pragma once
#include <vector>
#include <utility>



namespace snn {
	namespace internal {
		typedef std::vector<std::pair<std::vector<float>, std::vector<float>>> Set;
	}

	class Dataset {
	public:
		Dataset() = default;
		explicit Dataset(const std::size_t& size) noexcept;

		void shuffle();
		void clear();
		[[nodiscard]] std::size_t size() const;

		std::pair<std::vector<float>, std::vector<float>>& operator[](std::size_t index);
		const std::pair<std::vector<float>, std::vector<float>>& operator[](std::size_t index) const;

		void normalizeInputs(const float min = 0.f, const float max = 1.f);

	protected:
		void push(const std::vector<float>& input, const std::vector<float>& output);
		void push(const std::pair<std::vector<float>, std::vector<float>>& pair);

		internal::Set set;
	};
}