#pragma once
#include <SimpleNeuralNetwork/Dataset.hpp>
#include <string>
#include <fstream>

class MNIST : public snn::Dataset
{
public:
	MNIST() = delete;
	MNIST(const std::string& labels, const std::string& images, const std::size_t& outputSize = 10);

	void writeImage(const std::string& filename, const int index) const;


private:
	template<typename T>
	static T readBytes(std::ifstream& file);

	template<typename T>
	[[nodiscard]] static T swapEndian(T u);

};


