#pragma once
#include <SimpleNeuralNetwork/ActivationFunctions.hpp>
#include <cmath>

namespace snn {

	namespace Activation
	{
		inline float sigmoidFunction(const float x)
		{
			return 1.0f / (1.0f + std::exp(-x));
		}
		inline float sigmoidFunctionDerivative(const float x)
		{
			return std::exp(-x) / std::pow((std::exp(-x) + 1.0f), 2);
		}
		inline ActivationFunctions sigmoid{ sigmoidFunction, sigmoidFunctionDerivative };

		inline float tanhFunction(const float x)
		{
			return std::tanh(x);
		}

		inline float tanhFunctionDerivative(const float x)
		{
			return 1 - std::tanh(x) * std::tanh(x);
		}

		inline ActivationFunctions tanh{ tanhFunction, tanhFunctionDerivative };


		inline float ReLUFunction(const float x)
		{
			return x > 0 ? x : 0;
		}

		inline float ReLUFunctionDerivative(const float x)
		{
			return  x > 0 ? 1.f : 0.f;
		}

		inline ActivationFunctions ReLU{ ReLUFunction, ReLUFunctionDerivative };



		inline float LeakyReLUFunction(const float x, const float alpha)
		{
			return x > 0 ? x : alpha * x;
		}

		inline float LeakyReLUFunctionDerivative(const float x, const float alpha)
		{
			return  x > 0 ? 1 : alpha;
		}

		inline ActivationFunctions leakyReLU(float alpha)
		{
			return ActivationFunctions{
				std::bind(LeakyReLUFunction, std::placeholders::_1, alpha),
				std::bind(LeakyReLUFunctionDerivative, std::placeholders::_1, alpha)
			};
		};


	}
}