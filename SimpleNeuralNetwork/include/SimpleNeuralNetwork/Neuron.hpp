#pragma once
#include <SimpleNeuralNetwork/ActivationFunctions.hpp>
#include <fstream>
#include <vector>
#include <memory>

namespace snn
{

	constexpr float ALPHA = 0.01f;
	class Neuron
	{
	private:
		std::vector<float> lastInputs;
		std::vector<float> weights;
		float z;
		float bias = 1.f;
		std::shared_ptr<ActivationFunctions> activationFunctions;
		
	
	public:
		Neuron() = default;
		explicit Neuron(const unsigned& inputSize, const ActivationFunctions& activation);
		Neuron(const Neuron& neuron) = default;
		Neuron(Neuron&& neuron) = default;
		~Neuron() = default;
		
		Neuron& operator=(const Neuron&) = default;
		Neuron& operator=(Neuron&&) = default;
		
		[[nodiscard]] float feedForward(const std::vector<float>& inputs);
		[[nodiscard]] std::vector<float> backProp(float cost) noexcept;

		void setActivationFunctions(const ActivationFunctions& activation);
		
		void write(std::ofstream& file);
		void read(std::ifstream& file);
	};
}