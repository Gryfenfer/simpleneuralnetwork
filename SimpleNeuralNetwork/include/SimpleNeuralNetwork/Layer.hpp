#pragma once
#include <SimpleNeuralNetwork/Neuron.hpp>

namespace snn
{
	class Layer
	{
	private:
		std::vector<Neuron> neurons;
		unsigned nbInputs;

	public:
		Layer() = default;
		Layer(const unsigned& nbInputs, const unsigned& nbNeurons, const ActivationFunctions& activation);

		[[nodiscard]] std::vector<float> feedForward(const std::vector<float>& inputs);
		[[nodiscard]] std::vector<float> backProp(const std::vector<float>& inputs);

		void setActivationFunctions(const ActivationFunctions& activation);

		void write(std::ofstream& file);
		void read(std::ifstream& file);
	};
}
