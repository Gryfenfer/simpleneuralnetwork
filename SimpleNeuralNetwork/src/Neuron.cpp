#include <SimpleNeuralNetwork/Neuron.hpp>
#include <cmath>
#include <iostream>
#include <random>

using namespace snn;

Neuron::Neuron(const unsigned& inputSize, const ActivationFunctions& activation) {
	this->weights.resize(inputSize);
	z = 0.f;
	const float valueMax = std::sqrt(2.4f / static_cast<float>(inputSize));
	for (auto& weight : weights) {
		weight = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * 2 * valueMax - valueMax;
	}
	this->activationFunctions = std::make_shared<ActivationFunctions>(activation);
}

float Neuron::feedForward(const std::vector<float>& inputs)
{
	this->lastInputs = inputs;
	this->z = 0.f;
	for (std::size_t i = 0; i < inputs.size(); i++)
	{
		z += inputs[i] * weights[i];
	}
	z += bias;
	return activationFunctions->activationFunction(z);
}

std::vector<float> Neuron::backProp(float cost) noexcept
{
	cost *= activationFunctions->activationFunctionDerivative(z);
	std::vector<float> costs(this->weights.size());

	for (std::size_t i = 0; i < this->weights.size(); ++i)
	{
		costs[i] = cost * weights[i];

		//update weights
		const float deltaWeight = cost * this->lastInputs[i];
		this->weights[i] += deltaWeight * ALPHA;

	}
	return costs;
}

void Neuron::setActivationFunctions(const ActivationFunctions& activation)
{
	this->activationFunctions = std::make_shared<ActivationFunctions>(activation);
}

void Neuron::write(std::ofstream& file)
{
	std::size_t size = weights.size();
	file.write((char*)&size, sizeof(decltype(size)));

	for (std::size_t i = 0; i < size; i++) {
		float temp = weights[i];
		file.write((char*)&temp, sizeof(decltype(temp)));
	}
	for (std::size_t i = 0; i < size; i++) {
		float temp = lastInputs[i];
		file.write((char*)&temp, sizeof(decltype(temp)));
	}

	std::size_t z_b = z;
	file.write((char*)&z_b, sizeof(decltype(z_b)));

	std::size_t bias_b = bias;
	file.write((char*)&bias_b, sizeof(decltype(bias_b)));

}

void Neuron::read(std::ifstream& file)
{
	std::size_t size;
	file.read((char*)&size, sizeof(decltype(size)));
	weights.resize(size);
	lastInputs.resize(size);

	for (std::size_t i = 0; i < size; i++) {
		float temp;
		file.read((char*)&temp, sizeof(decltype(temp)));
		weights[i] = temp;
	}
	for (std::size_t i = 0; i < size; i++) {
		float temp;
		file.read((char*)&temp, sizeof(decltype(temp)));
		lastInputs[i] = temp;
	}
	float z_b;
	file.read((char*)&z_b, sizeof(decltype(z_b)));
	z = z_b;

	float bias_b;
	file.read((char*)&bias_b, sizeof(decltype(bias_b)));
	bias = bias_b;
}

