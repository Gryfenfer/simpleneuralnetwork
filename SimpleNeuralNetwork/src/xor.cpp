#include <SimpleNeuralNetwork/xor.hpp>
#include <iostream>

using namespace snn;

Xor::Xor() noexcept: Dataset::Dataset(){
    std::vector<std::vector<float>> input = {
        {0, 0},
        {1, 0},
        {0, 1},
        {1, 1}
    };
    std::vector<std::vector<float>> output = {
        {0},
        {1},
        {1},
        {0}
    };

    for(int i=0;i<4;++i){
        push(input[i], output[i]);
    }

    
    

}