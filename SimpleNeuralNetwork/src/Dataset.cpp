#include <SimpleNeuralNetwork/Dataset.hpp>
#include <random>
#include <algorithm>

using namespace snn;

Dataset::Dataset(const std::size_t& size) noexcept : set(size) {
}

void Dataset::clear() {
	this->set.clear();
}

void Dataset::shuffle() {
	std::random_device rd;
	std::mt19937 g(rd());
	std::shuffle(set.begin(), set.end(), g);
}

std::size_t Dataset::size() const {
	return set.size();
}

void Dataset::push(const std::vector<float>& input, const std::vector<float>& output) {
	this->set.push_back(std::make_pair(input, output));
}

void Dataset::push(const std::pair<std::vector<float>, std::vector<float>>& pair)
{
	this->set.push_back(pair);
}

void snn::Dataset::normalizeInputs(const float min, const float max)
{
	const std::size_t size = std::get<0>(set[0]).size();
	for (std::size_t i = 0; i < size; i++) {
		float maxElement = std::get<0>(set[0])[i];
		for (auto& [inputs, outputs] : set) {
			if (max < inputs[i]) maxElement = inputs[i];
		}
		for (auto& [inputs, outputs] : set) {
			inputs[i] = inputs[i] / maxElement * (max - min) - min;
		}
	}
}

std::pair<std::vector<float>, std::vector<float>>& Dataset::operator[](const std::size_t index) {
	return set[index];
}

const std::pair<std::vector<float>, std::vector<float>>& Dataset::operator[](const std::size_t index) const {
	return set[index];
}