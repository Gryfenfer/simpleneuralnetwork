#include <algorithm>
#include <SimpleNeuralNetwork/Perceptron.hpp>
#include <iostream>
#include <cassert>

using namespace snn;

Perceptron::Perceptron(std::vector<unsigned>& topology, const ActivationFunctions& activationFunctions)
{
	for (auto it = std::begin(topology) + 1; it != std::end(topology); ++it) {
		this->layers.emplace_back(*(it - 1), *it, activationFunctions);
	}
}

Perceptron::Perceptron(const std::string& filename, const ActivationFunctions& activationFunctions)
{
	read(filename);
	for (auto& layer : layers) {
		layer.setActivationFunctions(activationFunctions);
	}
}

std::vector<float> Perceptron::evaluate(const std::vector<float>& input)
{
	std::vector<float> out = this->layers.front().feedForward(input);
	for (auto it = std::begin(layers) + 1; it != std::end(layers); ++it)
	{
		out = it->feedForward(out);
	}
	return out;
}

std::vector<float> Perceptron::computeCost(const std::vector<float>& result, const std::vector<float>& targets)
{
	std::vector<float> cost(result.size());
	std::transform(result.begin(), result.end(), targets.begin(), cost.begin(), [](const float res, const float target) -> float
		{ return -2 * (res - target); });
	return cost;
}

void Perceptron::backProp(const std::vector<float>& result, const std::vector<float>& targets)
{
	std::vector<float> costs = computeCost(result, targets);
	for (auto it = std::rbegin(this->layers); it != std::rend(this->layers); ++it)
	{
		costs = it->backProp(costs);
	}
}

void Perceptron::train(Dataset& dataset, const unsigned nbEpoch)
{
	for (unsigned epoch = 0; epoch < nbEpoch; ++epoch)
	{
		for (std::size_t i = 0; i < dataset.size(); ++i)
		{
			const auto& [input, output] = dataset[i];
			auto out = evaluate(input);
			backProp(out, output);
		}

		dataset.shuffle();
	}
}

float Perceptron::test(const Dataset& dataset, const Problem problem) {
	float accuracy = 0.f;
	for (std::size_t i = 0; i < dataset.size(); ++i)
	{
		const auto& [input, output] = dataset[i];
		auto out = evaluate(input);

		switch(problem)
		{
		case Problem::Classification:
			if (std::max_element(out.begin(), out.end()) - out.begin() == std::max_element(output.begin(), output.end()) - output.begin()) { // We look for the index of the largest element
				accuracy += 1.f;
			}
			break;
		case Problem::Evaluation:
			for (auto& el : out) {
				el = el > 0.5f ? 1 : 0;
			}
			break;
		}
		
		if (out == output) accuracy += 1.f;
	}
	accuracy /= static_cast<float>(dataset.size());
	return accuracy;
}

void Perceptron::write(const std::string& filename)
{
	std::ofstream file{ filename,std::ios::binary };

	std::size_t size = layers.size();
	file.write((char*)&size, sizeof(decltype(size)));

	for (std::size_t i = 0; i < size; i++) {
		layers[i].write(file);
	}
}

void Perceptron::read(const std::string& filename)
{
	std::ifstream file{ filename, std::ios::binary };
	assert(file && "Cannot read the neural network file");
	std::size_t size;
	file.read((char*)&size, sizeof(decltype(size)));
	layers.resize(size);
	for (std::size_t i = 0; i < size; i++) {
		layers[i].read(file);
	}
}
