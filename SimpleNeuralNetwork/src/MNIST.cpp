#include <algorithm>
#include <cassert>
#include <SimpleNeuralNetwork/MNIST.hpp>
#include <climits>
#include <iostream>

using namespace snn;

MNIST::MNIST(const std::string& labels, const std::string& images, const std::size_t& outputSize)
{
	std::ifstream labelFile{ labels, std::ios::binary };
	std::ifstream imageFile{ images, std::ios::binary };

	uint32_t labelMagicNumber = readBytes<uint32_t>(labelFile);
	uint32_t imageMagicNumber = readBytes<uint32_t>(imageFile);

	uint32_t numberOfItems = readBytes<uint32_t>(labelFile);
	uint32_t NumberOfImages = readBytes<uint32_t>(imageFile);
	assert(numberOfItems == NumberOfImages);

	uint32_t numberOfRows = readBytes<uint32_t>(imageFile);
	uint32_t numberOfColumns = readBytes<uint32_t>(imageFile);
	uint32_t imageSize = numberOfRows * numberOfColumns;

	for (uint32_t i = 0; i < numberOfItems; i++)
	{
		uint8_t label = readBytes<uint8_t>(labelFile);
		std::vector<float> output(outputSize, 0.f);
		output[label] = 1.f;

		std::vector<float> input(imageSize);
		std::generate(input.begin(), input.end(), [&imageFile]() -> auto {return static_cast<float>(255 - readBytes<uint8_t>(imageFile)) / 255.f; });
		push(input, output);
	}
	labelFile.close();
	imageFile.close();
}

void MNIST::writeImage(const std::string& filename, const int index) const
{
	const auto& [image, label] = set[index];
	auto labelMax = std::max_element(label.begin(), label.end()) - label.begin();
	std::ofstream file{ filename + static_cast<char>('0' + labelMax) + ".pgm" };
	file << "P2\n";
	file << "28 28\n";
	file << "255\n";
	for (const auto& pixel : image)
	{
		file << pixel * 255 << " ";
	}
}


template<typename T>
T MNIST::readBytes(std::ifstream& file)
{
	T var;
	file.read(reinterpret_cast<char*>(&var), sizeof(T));
	return swapEndian<T>(var);
}

template<typename T>
T MNIST::swapEndian(T u) // https://stackoverflow.com/a/4956493/15560180
{
	static_assert (CHAR_BIT == 8, "CHAR_BIT != 8");

	union
	{
		T u;
		unsigned char u8[sizeof(T)];
	} source, dest;

	source.u = u;

	for (size_t k = 0; k < sizeof(T); k++)
		dest.u8[k] = source.u8[sizeof(T) - k - 1];

	return dest.u;
}
