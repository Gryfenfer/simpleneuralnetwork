#include <SimpleNeuralNetwork/Layer.hpp>
#include <iostream>
#include <future>

using namespace snn;

Layer::Layer(const unsigned& nbInputs, const unsigned& nbNeurons, const ActivationFunctions& activation)
{
	this->nbInputs = nbInputs;
	neurons.resize(nbNeurons, Neuron(nbInputs, activation));
}

std::vector<float> Layer::feedForward(const std::vector<float>& inputs)
{
	std::vector<float> output(this->neurons.size());
	std::transform(neurons.begin(), neurons.end(), output.begin(), [&inputs](Neuron& neuron) -> float {return neuron.feedForward(inputs); });
	return output;
}

std::vector<float> Layer::backProp(const std::vector<float>& inputCosts)
{

	std::vector<float> costs(nbInputs, 0.f);

	for (std::size_t i = 0; i < neurons.size(); i++)
	{
		auto costForOneNeuron = neurons[i].backProp(inputCosts[i]);
		for (std::size_t j = 0; j < costForOneNeuron.size(); j++)
		{
			costs[j] += costForOneNeuron[j];
		}
	}
	return costs;
}

void snn::Layer::setActivationFunctions(const ActivationFunctions& activation)
{
	for (auto& neuron : neurons) {
		neuron.setActivationFunctions(activation);
	}
}

void Layer::write(std::ofstream& file)
{
	std::size_t size = neurons.size();
	file.write((char*)&size, sizeof(decltype(size)));

	unsigned nbInputs_b = this->nbInputs;
	file.write((char*)&nbInputs_b, sizeof(decltype(nbInputs_b)));

	for (int i = 0; i < size; i++) {
		neurons[i].write(file);
	}
}

void snn::Layer::read(std::ifstream& file)
{
	std::size_t size;
	file.read((char*)&size, sizeof(decltype(size)));
	neurons.resize(size);

	unsigned nbInputs_b;
	file.read((char*)&nbInputs_b, sizeof(decltype(nbInputs_b)));
	nbInputs = nbInputs_b;

	for (int i = 0; i < size; i++) {

		neurons[i].read(file);
	}
}
