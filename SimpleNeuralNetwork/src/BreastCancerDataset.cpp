#include <SimpleNeuralNetwork/BreastCancerDataset.hpp>
#include <fstream>
#include <iostream>
using namespace snn;

BreastCancerDataset::BreastCancerDataset(const std::string& filename)
{
	std::ifstream file{ filename };
	constexpr std::size_t size = 116;
	for (std::size_t i = 0; i < size; i++) {
		std::vector<float> inputs(9);
		std::vector<float> output(2, 0.f);
		for (auto& input : inputs) {
			std::string temp;
			file >> temp;
			input = std::stof(temp); // conversion string -> float
		}
		int out;
		file >> out;
		output[out - 1] = 1.f;
		push(inputs, output);
	}
	normalizeInputs();
}

std::pair<BreastCancerDataset, BreastCancerDataset> BreastCancerDataset::split(float ratio)
{
	this->shuffle();
	BreastCancerDataset trainingDataset;
	for (int i = 0; i < ratio * this->size(); i++) {
		trainingDataset.push(this->operator[](i));
	}
	BreastCancerDataset testingDataset;
	for (int i = ratio * this->size(); i < this->size(); i++) {
		testingDataset.push(this->operator[](i));
	}
	return std::make_pair(trainingDataset, testingDataset);
}


